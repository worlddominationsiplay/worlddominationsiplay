﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldDomination.Engine;

namespace WorldDomination.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            var spy = new WebSpy();

            var path = Environment.CurrentDirectory + "\\" + DateTime.Now.ToString("yyyyMMddhhmm") + ".txt";

            System.Console.WriteLine("Please, enter the webpage you want to conquer and hit enter");
            string url = System.Console.ReadLine();

            if(string.IsNullOrWhiteSpace(url))
            {
                System.Console.WriteLine();
                System.Console.WriteLine("You haven't specify any page :(... but wait!!! I will use this instead: http://www.siplay.com/leagues");
                url = "http://www.siplay.com/leagues";
            }

            System.Console.WriteLine("This might take some minutes...");
            System.Console.WriteLine("Processing...");
            

            using (var report = File.CreateText(path))
            {
                string output = spy.GetSiteHierarchy(url, 0);
                report.Write(output);
            }

            Process.Start(path);

            System.Console.WriteLine("Hit enter key to finish the process.");

            System.Console.ReadLine();
        }
    }
}
