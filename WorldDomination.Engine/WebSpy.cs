﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using WorldDomination.Engine.Resources;

namespace WorldDomination.Engine
{
    public class WebSpy
    {
        /// <summary>
        /// Gets all links of a given URL
        /// </summary>
        /// <param name="url"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public string GetSiteHierarchy(string url, int level = 0)
        {
            StringBuilder sb = new StringBuilder();

            Uri uri;

            if (!(Uri.TryCreate(url, UriKind.Absolute, out uri) && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps)))
            {
                return string.Empty;
            }

            using (var client = new WebClient())
            {

                try
                {
                    // some sites redirects in case we don't give'em any browser identification
                    client.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";

                    // we expose the base level.
                    if (level == 0)
                    {
                        sb.Append(url);
                        sb.Append(Environment.NewLine);
                    }

                    // If we get here, then means we have diged in deeper.
                    level++;

                    // This is the third level, and we just return nothing.
                    if (level > 3)
                    {
                        return string.Empty;
                    }

                    // We set the level in a visual way using tabs.
                    string deepString = this.SetTab(level);

                    List<string> childUrls = GetPageLinks(uri.AbsoluteUri, client.DownloadString(uri.AbsoluteUri));

                    // I have just change this, in order to earn some performance.
                    var tasks = childUrls.Select(p => Task.Factory.StartNew(() =>
                    {
                        sb.Append(deepString);
                        sb.Append(p);
                        sb.Append(Environment.NewLine);

                        sb.Append(GetSiteHierarchy(p, level));
                    })).ToArray();

                    Task.WaitAll(tasks);

                    return sb.ToString();
                }
                catch(WebException webex)
                {
                    return "Exception: " + webex.Message;
                }
            }
        }

        /// <summary>
        /// Returns n tabs per repetitions specified.
        /// </summary>
        /// <param name="repetitions">Number of repetitions should allowed</param>
        /// <returns>Returns tabs per repetitions specified</returns>
        private string SetTab(int repetitions)
        {
            var sb = new StringBuilder();

            for(int i = 0; i<repetitions; i++)
            {
                sb.Append("\t");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Extract any markup document referenced into an anchor tag.
        /// </summary>
        /// <param name="currentUrl">Used to complete those links that do not have a host specified.</param>
        /// <param name="htmlSource">HTML source code to evaluate</param>
        /// <returns>List of links</returns>
        private List<string> GetPageLinks(string currentUrl, string htmlSource)
        {
            List<string> links = new List<string>();

            foreach(Match m in new Regex(StringResources.AnchorRegularExpression).Matches(htmlSource))
            {
                // there is no sense to declare outside this loop, every time we have a new value, the mem address value is destroyed and re-created.
                string extractedUrl = m.Groups[3].Value.ToLower().Trim();
                

                // I avoided to include this into regular expression because it wasn't performant at all, and testing that was taking more time than pre-assigned.
                if(extractedUrl.Contains(".css") | extractedUrl.Contains(".js") | extractedUrl.Contains(".pdf") 
                    | extractedUrl.Contains(".gif") | extractedUrl.Contains(".jpg") | extractedUrl.Contains(".jpeg") 
                    | extractedUrl.Contains(".zip") | extractedUrl.Contains(".ico") | extractedUrl.Contains("mailto:"))
                {
                    continue;
                }


                // used for those cases where the link is without host part of uri.
                if (extractedUrl.IndexOf("http") == -1)
                {
                    extractedUrl = (currentUrl + extractedUrl).Replace("//", "/");
                }

                links.Add(extractedUrl);
            }

            // avoid duplicated elements.
            return links.Distinct().ToList();
        }  
    }
}


